import Actions from './actions';
import Mutations from './mutations';
import Getters from './getters';
import State from './state';

export default {
    namespaced: true,
    actions: Actions,
    mutations: Mutations,
    getters: Getters,
    state: State,

}