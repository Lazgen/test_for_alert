import jsonpatch from 'jsonpatch';

export default {

    jsonPatch: async ( state, data ) => {
        let patch = data;
        let d = JSON.stringify(state.data);
        let json = jsonpatch.apply_patch(d, patch);
        state.data = JSON.parse(json);
        consople.log(state.data);
    }

}
