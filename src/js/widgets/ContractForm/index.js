import Vue from 'vue';
import Vuex from 'vuex';
import store from '../../store/store';
import Contract from './index.vue';

Vue.use(store);

export default class ContractForm {

    constructor (selector) {
        let items = document.querySelectorAll(selector);
        items.forEach(item => {
            new Vue({
                components: { 
                    Contract
                },
                store,
                template: '<Contract/>',
            }).$mount(item);
        })
    }
}