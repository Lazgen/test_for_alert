import '../sass/main.scss';

import '../assets/down-arrow.svg';


import Vue from "vue";
import Vuex from "vuex";

import Noty from 'noty';

Vue.use(Vuex);

import ContractForm from './widgets/ContractForm/index.js';

let contractForm = new ContractForm('#contract');