// MAIN INCLUDES
const webpack = require("webpack");
const path = require("path");
const fs = require("fs");

// OTHER INCLUDES
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require("autoprefixer");

const { VueLoaderPlugin } = require("vue-loader");

// WEBPACK LOGICK
module.exports = (env, options)=>{

    // PATHSET
    const PATH = env === 'prod'
        ? path.resolve(__dirname, 'build')
        : path.resolve(__dirname, 'dist');
    
    // ENTERY POINTS GENERATION
    const PATH_DEV_ROOT = path.resolve(__dirname, 'src');

    /**
     * 
     * @param {string} PATH is path to directory
     * @param {object} ENTRY_OBJ is results object: { 
     * enty: {key: full_filename, key: full_filename}, 
     * output: {key: full_filename,key: full_filename}
     * } 
     * key -> filename without extension, filename -> full filename
     * @param {regexp} extReg extention what we will check for results
     * @param {string} outputExt extention of output file
     */
    //add output extension
    function getEntrys(PATH_INTO, PATH_TO, OBJ, extReg, outputExt) {

        let reg = extReg;
        let arr = fs.readdirSync(PATH_INTO);

        if (OBJ.entry === undefined) OBJ.entry = {};
        if (OBJ.output === undefined) OBJ.output = {};

        arr.forEach(file => {
            name = file.replace(reg,'');
            if (reg.test(file)) {
                OBJ.entry[name] = path.resolve(PATH_INTO, file);
                if (outputExt)
                    OBJ.output[name] = path.resolve(PATH_TO, file).replace(reg, outputExt);
                    
            }
        });
    };
    
    const JS_ENTRYS = {}

    getEntrys(
        path.resolve(PATH_DEV_ROOT,'js'),
        path.resolve(PATH,'js'),
        JS_ENTRYS,
        /\.js$/);

    // PUG-HTML BUILD QUERY
    const PUG_HTML = {}
    getEntrys(
        path.resolve(PATH_DEV_ROOT,'pages'),
        path.resolve(PATH, 'pages'),
        PUG_HTML,
        /\.pug$/,
        '.html');
        
    getEntrys(
        path.resolve(PATH_DEV_ROOT,'pages'),
        path.resolve(PATH, 'pages'),
        PUG_HTML,
        /\.pug$/,
        '.html');
    let filelist = fs.readdirSync(path.resolve(PATH_DEV_ROOT, 'pages'))
    
    console.log("FILELIST BEFORE: ", filelist);
    

    var irm = [];
    filelist.forEach(file => {
        let reg = /\.(pug|html)$/
        console.log(reg.test(file), filelist.indexOf(file))
        if (reg.test(file))
            irm.push(filelist.indexOf(file));
    });

    console.log(filelist)
    console.log(irm)

    for (let i = irm.length-1; i >= 0; i--) {
        filelist.splice(irm[i], 1);
    }

    console.log("FILELIST AFTER: ", filelist);

    let path_to_pages_input = path.resolve(PATH_DEV_ROOT, 'pages');
    let path_to_pages_output = path.resolve(PATH, 'pages');
    
    filelist.forEach(name => {
        getEntrys(
            path.resolve(path_to_pages_input, name),
            path.resolve(path_to_pages_output, name),
            PUG_HTML,
            /\.pug$/,
            '.html');
    });
    console.log(PUG_HTML);
    PUG_HTML_PLUGINS = [];

    Object.keys(PUG_HTML.entry).forEach(path_name => {
        PUG_HTML_PLUGINS.push(
            new HtmlWebpackPlugin({
                hash: false,
                template: PUG_HTML.entry[path_name],
                filename: PUG_HTML.output[path_name]
                    .replace(/([\S\s]*)(?=pages\/)/,'')
                    .replace('.pug', '.html'),
                pretty: true,
                minify: false
            })
        )
    });

    getEntrys(
        PATH_DEV_ROOT,
        PATH,
        PUG_HTML,
        /\.html$/,
        '.html'
    );
    


    console.log(PUG_HTML_PLUGINS)
    
    // PLUGINS PREBUILD
    const POSTCSS_PLUGINS =  [
        autoprefixer({
            browsers: [
                'ie >= 8',
                'last 4 version',
            ]

        })
    ]
    if (env === "prod") 
        POSTCSS_PLUGINS.push(require('cssnano'));

    // CONFIG ASSEMBLING
    const conf = {
    
        entry: JS_ENTRYS.entry,
        output: {
            path: PATH,
            filename: 'js/[name].js',
            library: '[name]'
        },

        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.esm.js',
                extensions: '*|.js|.vue|.json'
            }
        },

        plugins: [
            new ExtractTextPlugin(
                'css/[name].css',
                {allChunks: true}),
           new HtmlWebpackPlugin({
                hash: false,
                template: 'src/index.html',
                filename: 'index.html',
                pretty: true,
                minify: false
            }),
            new VueLoaderPlugin(),
        ].concat(PUG_HTML_PLUGINS),

        module : {
            rules: [
                
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader'
                    }
                },

                {
                    test: /\.vue$/,
                    loader: 'vue-loader',
                },

                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    loader: 'file-loader',
                    options: {
                        name: "fonts/[name].[ext]",
                    }
                },
                {
                    test: /\.pug$/,
                    use: [
                        {
                            loader: "html-loader",
                        },
                        {
                            loader: "pug-html-loader",
                            options: {
                                doctype: 'html',
                                pretty: env === "prod" ? false : true
                            }
                        }
                    ]
                },
                {
                    test: /\.(svg|png|jpg|ico)$/,
                    loader: "file-loader",
                    options: {
                        name: 'assets/[name].[ext]'
                    }
                },
                {
                    test: /\.(sass|scss|css)$/,
                    use: ExtractTextPlugin.extract({
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    url: false,
                                    sourceMap: true
                                }
                            },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    plugins: POSTCSS_PLUGINS
                                }
                            },
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: true
                                }
                            },
                        ]
                    })
                },
            ]
        }
    }

    return conf;
}
